import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'app/service/authentication.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent {


  constructor(private authenticationService: AuthenticationService, private router: Router) { }

  public checkLogin(): Boolean {
    return this.authenticationService.isLoggedIn()
  }

  ngOnInit() {
    console.log("in dashboard value :"+this.authenticationService.isLoggedIn())
    if (!this.authenticationService.isLoggedIn()) {
      console.log("redirecting")
      this.router.navigate(['/login']);
    }
  }

}
