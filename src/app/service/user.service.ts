import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import { AuthenticationService } from './authentication.service';
import {Router,ActivatedRoute} from '@angular/router';


@Injectable()
export class UserService {

    private findUserApi = 'api/me';

    authenticationService: AuthenticationService;


    constructor(private http: Http, authenticationService: AuthenticationService,private router
    :Router) {
        this.authenticationService = authenticationService;
    }

    getUser(): Observable<Response> {

        return this.http.get(this.findUserApi)
            .map(this.extractData)
            .catch(this.handleError);
    }

    private handleError(error: Response | any) {
        // In a real world app, you might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.log(errMsg);
        return Observable.throw(errMsg);
    }

    private extractData(res: Response) {
        let body = res.json();
        return body.data || {};
    }


    public loginUser(username: String, password: String) {
        this.authenticationService.loginUser(username, password).subscribe(
            data => {
                    // login successful so redirect to return url
                    //this.router.navigateByUrl('/dashboard');
                     this.router.navigate(['/dashboard'])
                }
        );
       

    }

}
