import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';

@Injectable()
export class AuthenticationService {

    private authToken: String;
    private loginUserApi = 'http://127.0.0.1:8888/login';


    constructor(private http: Http) {

    }


    loginUser(username: String, password: String) {
        let headers = new Headers({ 'Content-type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        var loginCredentials = JSON.stringify({ 'username': username, 'password': password });
        return this.http.post(this.loginUserApi, loginCredentials, options)
            .map(this.extractTokenHeader)
            .catch(this.handleError);
    }

    private extractTokenHeader(res: Response) {
        this.authToken = res.headers.get('Authorization');
        localStorage.setItem("Authorization", res.headers.get('Authorization'));
        console.log("token value "+this.authToken);
        //return false;
    }

    public isLoggedIn(): Boolean {
        
        if (localStorage.getItem('Authorization')==null) {
            return false;
        }
        return true;
    }

    private handleError(error: Response | any) {
        // In a real world app, you might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.log(errMsg);
        return Observable.throw(errMsg);
    }

    public setAuthToken(authToken: String) {
        this.authToken = authToken;
    }

    public getAuthToken(): String {
        return this.authToken;
    }


}