import {NgModule} from '@angular/core';
import {RouterModule,Routes} from '@angular/router';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { DashboardComponent } from './dashboard/dashboard.component';



const appRoutes: Routes=[

  {path:'',redirectTo:'login',pathMatch:'full'},
  {path:'login',component:LoginFormComponent},
  {path:'dashboard',component:DashboardComponent},
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
imports:[
    RouterModule.forRoot(appRoutes)
    ],
    exports:[
        RouterModule
    ]

})
export class AppRoutingModule{}