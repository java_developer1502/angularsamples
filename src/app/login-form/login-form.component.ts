import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ValidationService } from './validation.service';
import { UserService } from 'app/service/user.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent {

  userForm: any;

  userService: UserService;

  constructor(private formBuilder: FormBuilder, userService: UserService) {

    this.userService = userService;

    this.userForm = this.formBuilder.group({
      'username': ['', Validators.required],
      'password': ['', [Validators.required]]
    });
    console.log(this.userForm);

  }

  loginUser() {
    if (this.userForm.dirty && this.userForm.valid) {
      alert(`Name: ${this.userForm.value.username} Email: ${this.userForm.value.password}`);
      this.userService.loginUser(this.userForm.value.username, this.userForm.value.password);
    }
  }

}
