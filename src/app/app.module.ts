import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { LoginFormComponent } from './login-form/login-form.component';
import {ValidationService} from './login-form/validation.service';
import { ControlMessagesComponent } from './login-form/control-message.component';
import {UserService} from './service/user.service';
import {AuthenticationService} from './service/authentication.service';
import { DashboardComponent } from './dashboard/dashboard.component';
import {RouterModule,Routes} from '@angular/router';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import {AppRoutingModule}  from './app-routing.module';


// const routes: Routes=[

//   {path:'',redirectTo:'login',pathMatch:'full'},
//   {path:'login',component:LoginFormComponent},
//   {path:'dashboard',component:DashboardComponent},
//   { path: '**', component: PageNotFoundComponent }
// ];


@NgModule({
  declarations: [
    AppComponent,
    ControlMessagesComponent,
    LoginFormComponent,
    DashboardComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    // RouterModule.forRoot(routes),
    ReactiveFormsModule,
    FormsModule,
    HttpModule
  ],
  providers: [ValidationService,UserService,AuthenticationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
    